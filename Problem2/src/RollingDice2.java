//********************************************************************
//File:         RollingDice2.java       
//Author:       YANSHUN QIU
//Date:         Oct. 14th
//Course:       CPS100
//
//Problem Statement:
//Using the Die class defined in this chapter, write a class called PairOfDice, 
//composed of two Die objects. Include methods to set and get the individual
//die values, a method to roll the dice, and a method that returns the current
//sum of the two die values. Create a driver class called RollingDice2 to 
//instantiate and use a PairOfDice.





//
//Inputs:   none
//Outputs:   sum of die1+die2
//********************************************************************


public class RollingDice2
{

  public static void main(String[] args)
  {
    PairOfDice roll = new PairOfDice();
    
    int sum = roll.die1.getFaceValue() + roll.die2.getFaceValue();
    
    System.out.println("Die1 :" + roll.die1.getFaceValue());
    System.out.println("Die2 :" + roll.die2.getFaceValue());

    System.out.println("The sum of the two die values :" + sum);
     
  }

}


public class Book {

	private String title;
	private String author;
	private String publisher;
	private int copyrightdate;
	
	public Book(String strTitle, String strAuthor, String strPublisher, int iCopyrightdate)
	{
		title = strTitle;
		author = strAuthor;
		publisher = strPublisher;
		copyrightdate = iCopyrightdate;
	}
	public void setTitle(String strTitle)
	{
		title = strTitle;
	}
	public void setAuthor(String strAuthor)
	{
		author = strAuthor;
	}
	public void setPublisher(String strPublisher)
	{
		publisher = strPublisher;
	}
	public void setCopyrightdate(int iCopyrightdate)
	{
		copyrightdate = Math.abs(iCopyrightdate);
	}
	public String getTitle()
	{
		return title;
	}
	public String getAuthor()
	{
		return author;
	}
	public String getPublisher()
	{
		return publisher;
	}
	public int getCopyrightdate()
	{
		return copyrightdate;
	}
	public String toString()
	{
		String strTmp = "";
		
		strTmp = strTmp + "Title: " + title;
		strTmp = strTmp + "Author: " + author;
		strTmp = strTmp + "Publisher: " + publisher;
		strTmp = strTmp + "Copyrightdate: " + copyrightdate;
		
		return strTmp;
	}
	
	
	


	

	
}

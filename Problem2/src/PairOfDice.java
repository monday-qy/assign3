
public class PairOfDice
{

  Die die1 = new Die();
  Die die2 = new Die();
  
  public PairOfDice()
  {
    roll();
  }
  public void setDie1FaceValue()
  {
    die1.setFaceValue((6)+1);
  }
  public Die getdie1()
  {
    return die1;
    
  }
  public void setDie2FaceValue()
  {
    die2.setFaceValue((6)+1);
  }
  public Die getdie2()
  {
    return die2;
  }
  public void roll()
  {
    die1.setFaceValue(die1.roll());
    die2.setFaceValue(die2.roll());

  }
  int sum = die1.getFaceValue() + die2.getFaceValue();
  public int getSum()
  {
    return sum;
    
    
  }
}

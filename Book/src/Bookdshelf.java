//********************************************************************
//File:         Bookshelf.java       
//Author:       YANSHUN QIU
//Date:         Oct. 14th
//Course:       CPS100
//
//Problem Statement:
//Write a class called Book that contains instance data for the 
//title, author, publisher, and copyright date. Define the Book
//constructor to accept and initialize data. Include setter and
//getter methods for all instance data. Include a toString method
//that returns a nicely formatted, multi-line description of the 
//book. Create a driver class called Bookshelf, whose main method 
//instantiates and updates several Book objects.
                            
//
//Inputs:   
//Outputs:  
//********************************************************************

public class Bookdshelf {

	public static void main(String[] args) {
		
		Book book1 = new Book("Citizen", "Anthony", "Picinini", 2005);
		Book book2 = new Book("Luckydt", "Damon", " Gifds", 2009);
		Book book3 = new Book("Demond", "Qianhe", "Gifds", 2012);
		
		System.out.println("Book = " + book1);
		System.out.println("Book = " + book2);
		System.out.println("Book = " + book3);


		
		book1.setTitle("Long Island");
		System.out.println("Book = " + book1);
		System.out.println("Title Setter: " + book1.getTitle());

		book2.setTitle("Fmndue");
		book2.setAuthor("Nbdehe");
		book2.setPublisher("Gitfs");
		book2.setCopyrightdate(2001);
		System.out.println("Book =" + book2);
		
		String strResult = book3.getTitle();
		System.out.println("Title = " + strResult);
		strResult = book3.getAuthor();
		System.out.println("Author =" + strResult);
		strResult = book3.getPublisher();
		System.out.println("Publisher =" + strResult);
		int age = book3.getCopyrightdate();
		System.out.println("Copyrightdate = " + age);

		

	}

}
